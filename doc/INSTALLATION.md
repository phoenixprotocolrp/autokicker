---
layout: default
title: Installation
---

1. Unzip `phoenix-autokicker.zip`.
2. Place `phoenix-autokicker` directory into the FiveM resources folder.
3. Ensure the autokicker starts by putting `ensure phoenix-autokicker` into your server configuration.
