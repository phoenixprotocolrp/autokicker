---
layout: default
title: Add a rule
---

All rules are defined in `config.lua`.

Rules can be added to the `AutokickerRules` table using:

```lua
AutokickerRules["my rule"] = "Player name cannot be 'my rule'"
```

`"my rule"` is a lua [pattern match](https://www.lua.org/pil/20.2.html) so you can get
creative with your rules.
