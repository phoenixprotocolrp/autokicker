---
layout: default
title: About
---

When a player joins the server, if they have a name that is not
up to your server's standards, boot them automatically.
