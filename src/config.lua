-- @copyright Phoenix Protocol - BSL-1.0
-- @author sircapsalot
-- @since Oct 4, 2023
-- this file contains the reasons for kick that the Player will see when being ejected from the server.
-- if the player name matches any rules in this table, the player will be automatically ejected.

-- you may add your rules to this file by setting a new AutokickerRules["<REGEX>"] = "<REASON>"
--   where <REGEX> will be the matching pattern to match to the player name (lowercased!)
--   where <REASON> will be the reason the player will see

AutokickerRules = {}

-- color in player name
AutokickerRules["~%a~"] = "Please rejoin without color in your name"

-- foul language
AutokickerRules["nigga"] = "Please rejoin with a name not so foul"
AutokickerRules["nigger"] = "Please rejoin with a name not so foul"
AutokickerRules["cracker"] = "Please rejoin with a name not so foul"
AutokickerRules["fuck"] = "Please rejoin with a name not so foul"
AutokickerRules["shit"] = "Please rejoin with a name not so foul"
AutokickerRules["tits"] = "Please rejoin with a name not so foul"
AutokickerRules["ass"] = "Please rejoin with a name not so foul"
AutokickerRules["bitch"] = "Please rejoin with a name not so foul"
AutokickerRules["cunt"] = "Please rejoin with a name not so foul"
AutokickerRules["twat"] = "Please rejoin with a name not so foul"

-- must match a specific format
-- AutokickerRules[".+ | .+ | (FBI|LSPD|SAFR|LCSO|SAHP)"] = "the reason for kick"

-- must have a first/last name
-- AutokickerRules["%a %a"] = "Please name your character with a first and last name"
