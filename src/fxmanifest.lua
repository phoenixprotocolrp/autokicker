-- @copyright Phoenix Protocol - BSL-1.0
-- @author sircapsalot
-- @since Oct 4, 2023

fx_version "bodacious"
game "gta5"

name "Autokicker"
description "Auto kick players based on their player name"
author "sircapsalot"
version "1.0.3"

server_scripts {
  "config.lua",
  "server.lua"
}
