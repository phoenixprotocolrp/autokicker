-- @copyright Phoenix Protocol - BSL-1.0
-- @author sircapsalot
-- @since Oct 4, 2023

AddEventHandler('playerConnecting', function(playerName, setKickReason, deferrals)
  deferrals.defer()

  Wait(0) -- mandatory

  deferrals.update("checking player name...")

  for match, reason in pairs(AutokickerRules) do
    if string.find(string.lower(playerName), match) then
      setKickReason(reason)
      deferrals.done(reason)
      break
    end
  end

  Wait(0)
  deferrals.done()
end)
